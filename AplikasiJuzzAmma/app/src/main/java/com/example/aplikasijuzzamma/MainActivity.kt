package com.example.aplikasijuzzamma

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_menu.setOnClickListener() {
            val intent = Intent(this@MainActivity, Menu::class.java)
            startActivity(intent)
        }
        btn_kiblat.setOnClickListener() {
            val intent = Intent(this@MainActivity, Kiblat::class.java)
            startActivity(intent)
        }
        btn_tentang.setOnClickListener() {
            val intent = Intent(this@MainActivity, Tentang::class.java)
            startActivity(intent)
        }

    }
}
