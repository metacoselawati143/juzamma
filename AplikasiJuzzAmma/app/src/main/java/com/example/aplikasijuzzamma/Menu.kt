package com.example.aplikasijuzzamma

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_menu.*

class Menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        alfatihah.setOnClickListener() {
            val intent = Intent(this@Menu, Alfatihah::class.java)
            startActivity(intent)
            annas.setOnClickListener() {
                val intent = Intent(this@Menu, Annas::class.java)
                startActivity(intent)}
        }
    }
}
